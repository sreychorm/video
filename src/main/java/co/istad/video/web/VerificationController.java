package co.istad.video.web;

import org.mapstruct.Mapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class VerificationController {
    @GetMapping("/html")
    String viewHtml(){
        return "mail-verification";
    }
}
