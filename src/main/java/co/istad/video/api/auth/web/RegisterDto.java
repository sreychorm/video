package co.istad.video.api.auth.web;

import java.time.LocalDateTime;

public record RegisterDto(
        String email,
        String familyName,
        String givenName,
        String gender,
        String password,
        String confirmedPassword
) {
}
