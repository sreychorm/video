package co.istad.video.api.auth;

import lombok.Data;
import org.apache.ibatis.jdbc.SQL;


public class AuthProvider {

    public String buildCreateSubscriberSql() {
        return new SQL() {{
            INSERT_INTO("users_roles");
            VALUES("user_id", "#{userId}");
            VALUES("role_id", "3");
        }}.toString();
    }

    public String buildRegisterSql() {
        return new SQL() {{
            INSERT_INTO("users");
            VALUES("uuid", "#{user.uuid}");
            VALUES("email", "#{user.email}");
            VALUES("family_name", "#{user.familyName}");
            VALUES("given_name", "#{user.givenName}");
            VALUES("gender", "#{user.gender}");
            VALUES("password", "#{user.password}");
            VALUES("created_at", "NOW()");
            VALUES("status", "#{user.status}");
        }}.toString();
    }

}
