package co.istad.video.api.auth;

import co.istad.video.api.auth.web.AuthDto;
import co.istad.video.api.auth.web.RegisterDto;
import co.istad.video.api.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;
@RequiredArgsConstructor
@Service
public class AuthServiceImpl implements AuthService{

    private final AuthMapper authMapper;
    private final AuthMapStruct authMapStruct;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    public AuthDto register(RegisterDto registerDto) {
        User user = authMapStruct.fromRegisterDto(registerDto);
        user.setUuid(UUID.randomUUID().toString());
        user.setStatus(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));


        authMapper.register(user);

        // Assign role - SUBSCRIBER
        authMapper.createSubscriber(user.getId());

        return authMapStruct.toAuthDto(user);

    }
}
