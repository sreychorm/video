package co.istad.video.api.auth;

import co.istad.video.api.auth.web.AuthDto;
import co.istad.video.api.auth.web.RegisterDto;

public interface AuthService {
    AuthDto register(RegisterDto registerDto);
}
