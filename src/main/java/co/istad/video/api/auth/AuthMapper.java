package co.istad.video.api.auth;

import co.istad.video.api.user.User;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper

public interface AuthMapper {
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    @InsertProvider(type = AuthProvider.class, method = "buildRegisterSql")
    void register(@Param("user") User user);

    @InsertProvider(type = AuthProvider.class, method = "buildCreateSubscriberSql")
    void createSubscriber(@Param("userId") Integer userId);


}
