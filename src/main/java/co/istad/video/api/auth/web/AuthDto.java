package co.istad.video.api.auth.web;

import java.time.LocalDate;
import java.time.LocalDateTime;

public record AuthDto(
        String email,
        String familyName,
        String givenName,
        String gender
        ) {

}
