package co.istad.video.api.auth;

import co.istad.video.api.auth.web.AuthDto;
import co.istad.video.api.auth.web.RegisterDto;
import co.istad.video.api.user.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthMapStruct {

    User fromRegisterDto(RegisterDto registerDto);

    AuthDto toAuthDto(User user);

}
