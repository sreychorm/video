package co.istad.video.api.user.web;

import co.istad.video.api.playlist.web.PlayListDto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public record UserDto(
        String email,
        String familyName,
        String givenName,
        String gender,
        List <RoleDto> roles,
        List <PlayListDto> playlists
) {
}
