package co.istad.video.api.user;

import co.istad.video.api.user.web.SaveUserDto;
import co.istad.video.api.user.web.UpdateUserDto;
import co.istad.video.api.user.web.UserDto;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
@Slf4j
public class UserServiceImpl implements UserService{

   private final UserMapper userMapper;
   private final UserMap userMap;
    @Override
    public UserDto saveUser(SaveUserDto saveUserDto) {
        User user = userMap.fromSaveUserDto(saveUserDto);
        user.setUuid(UUID.randomUUID().toString());

        userMapper.insert(user);

        log.info("User = {}", user);

        Optional<User> userOp = userMapper.selectById(user.getId());

        User latestUser = userOp.orElseThrow(() ->
                new RuntimeException("User is not found"));
        return userMap.toUserDto(latestUser);

    }

    public PageInfo<UserDto> findUsers(int pageNum, int pageSize) {

        PageInfo<User> userPageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> userMapper.select(true));

        return userMap.toUserDtoPageInfo(userPageInfo);
    }


    @Override
    public UserDto findUserByUuid(String uuid) {
        User user = userMapper.selectByUuid(uuid).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "User with this UUID is not exist in the db..!",
                        new Throwable("Something went wrong!")));
        return userMap.toUserDto(user);
    }

    @Override
    public void deleteByUuid(String uuid) {

        userMapper.selectByUuid(uuid)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND,
                                "User with this UUID is not exist in the db..!",
                                new Throwable("Something went wrong!")));

        userMapper.deleteByUuid(uuid);

    }

    public UserDto findPlaylistOfUserByUuid(String uuid) {
        User user = userMapper.selectPlaylistOfUserByUuid(uuid).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "User with this UUID is not exist in the db..!",
                        new Throwable("Something went wrong!")));
        return userMap.toUserDto(user);
    }

    @Override
    public UserDto updateByUuid(String uuid, UpdateUserDto updateUserDto) {
        return null;
    }
}
