package co.istad.video.api.user;

import co.istad.video.api.user.web.RoleDto;
import co.istad.video.api.user.web.SaveUserDto;
import co.istad.video.api.user.web.UserDto;
import com.github.pagehelper.PageInfo;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMap {
    User fromSaveUserDto(SaveUserDto saveUserDto);
    UserDto toUserDto(User user);

    PageInfo<UserDto> toUserDtoPageInfo(PageInfo<User> userPageInfo);

    RoleDto toRoleDto(Role role);
}
