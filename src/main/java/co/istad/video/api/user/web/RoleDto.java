package co.istad.video.api.user.web;

public record RoleDto(String name) {
}
