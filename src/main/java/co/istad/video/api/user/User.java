package co.istad.video.api.user;

import co.istad.video.api.playlist.PlayList;
import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class User {
    private Integer id;
    private String uuid;
    private String username;
    private String email;
    private String familyName;
    private String givenName;
    private LocalDate dob;
    private String gender;
    private String phone;
    private String profile;
    private String biography;
    private String password;
    private String verifiedToken;
    private LocalDateTime tokenExpiry;
    private LocalDateTime createdAt;
    private Boolean status;
    private List<Role> roles;
    private List<PlayList> playLists;


}
