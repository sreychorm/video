package co.istad.video.api.user.web;

import jakarta.validation.constraints.NotBlank;

import java.time.LocalDate;
import java.time.LocalDateTime;

public record SaveUserDto(
        @NotBlank(message = "You have to fill username..!")
        String username,

        @NotBlank(message = "You have to fill family name..!")
        String familyName,

        @NotBlank(message = "You have to fill given name..!")
        String givenName,
        String gender,
        LocalDate dob,

        @NotBlank(message = "You have to fill phone number..!")
        String phone,
        String biography,
        Boolean status


) {
}
