package co.istad.video.api.user;

import co.istad.video.api.user.web.SaveUserDto;
import co.istad.video.api.user.web.UpdateUserDto;
import co.istad.video.api.user.web.UserDto;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface UserService {

    UserDto saveUser(SaveUserDto saveUserDto);


    PageInfo<UserDto> findUsers(int pageNum, int pageSize);

    UserDto findUserByUuid(String uuid);

    void deleteByUuid(String uuid);

    UserDto updateByUuid(String uuid, UpdateUserDto updateUserDto);

}
