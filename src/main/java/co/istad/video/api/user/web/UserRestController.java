package co.istad.video.api.user.web;

import co.istad.video.api.auth.base.Rest;
import co.istad.video.api.user.UserServiceImpl;
import com.github.pagehelper.PageInfo;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserRestController {

    private final UserServiceImpl userService;

    @PostMapping
    Rest<?> saveUser(@Valid @RequestBody SaveUserDto saveUserDto) {

        var userDto = userService.saveUser(saveUserDto);

        return Rest.builder()
                .status(true)
                .message("You have retrieved user successfully")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(userDto)
                .build();
    }


    @GetMapping
    Rest<?> findUsers(@RequestParam(required = false, defaultValue = "20") int pageNum,
                      @RequestParam(required = false, defaultValue = "1") int pageSize) {

        PageInfo<UserDto> userDtoPageInfo = userService.findUsers(pageNum, pageSize);

        return Rest.builder()
                .status(true)
                .message("You have retrieved users successfully")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(userDtoPageInfo)
                .build();
    }

    @GetMapping("/{uuid}")
    Rest<?> findUserByUuid(@PathVariable("uuid") String uuid) {

        UserDto userDto = userService.findUserByUuid(uuid);

        return Rest.builder()
                .status(true)
                .message("You have retrieved an user successfully")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(userDto)
                .build();
    }

    @DeleteMapping("/{uuid}")
    Rest<?> deleteUserByUuid(@PathVariable("uuid") String uuid) {

        userService.deleteByUuid(uuid);

        return Rest.builder()
                .status(true)
                .message("You have deleted an user successfully")
                .code(HttpStatus.NO_CONTENT.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
    }


    @GetMapping("/{uuid}/playlists")
    Rest<?> findPlaylistsOfUserByUuid(@PathVariable("uuid") String uuid) {

        UserDto userDto = userService.findPlaylistOfUserByUuid(uuid);

        return Rest.builder()
                .status(true)
                .message("You have retrieved an user successfully")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(userDto)
                .build();
    }






}
