package co.istad.video.api.user;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    public String buildSelectByUsernameOrEmailSql() {
        return new SQL() {{
            SELECT("*");
            FROM("users");
            WHERE("username = #{usernameOrEmail}", "status = TRUE");
            OR();
            WHERE("email = #{usernameOrEmail}", "status = TRUE");
        }}.toString();
    }


    public String buildInsertSql(){
        return new SQL() {{
            INSERT_INTO("users");
            VALUES("uuid", "#{user.uuid}");
            VALUES("username", "#{user.username}");
            VALUES("family_name", "#{user.familyName}");
            VALUES("given_name", "#{user.givenName}");
            VALUES("gender", "#{user.gender}");
            VALUES("dob", "#{user.dob}");
            VALUES("phone", "#{user.phone}");
            VALUES("biography", "#{user.biography}");
            VALUES("status", "#{user.status}");

        }}.toString();
    }

    public String buildSelectByIdSql() {
        return new SQL() {{
            SELECT("*");
            FROM("users");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String buildSelectSql() {
        return new SQL() {{
            SELECT("*");
            FROM("users");
            WHERE("status = #{status}");
            ORDER_BY("id DESC");
        }}.toString();
    }

    public String buildSelectByUuidSql() {
        return new SQL() {{
            SELECT("*");
            FROM("users");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String buildSelectUserRoles() {
        return new SQL() {{
            SELECT("*");
            FROM("roles r");
            INNER_JOIN("users_roles ur ON ur.role_id = r.id");
            WHERE("ur.user_id = #{id}");
        }}.toString();
    }


    public String buildSelectPlaylistsOfUser() {
        return new SQL() {{
            SELECT("*");
            FROM("users u");
            INNER_JOIN("playlists p ON u.id = p.owner_id");
            WHERE("u.uuid = #{uuid}");
        }}.toString();
    }




}
