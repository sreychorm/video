package co.istad.video.api.file.web;

public record FileDto(
        String name,
        String uri,
        String extension,
        Long size
) {
}
