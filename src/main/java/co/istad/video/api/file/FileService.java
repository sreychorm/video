package co.istad.video.api.file;

import co.istad.video.api.file.web.FileDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {
    FileDto uploadOne(MultipartFile file);

    List<FileDto> uploadAll(List<MultipartFile> files);
}
