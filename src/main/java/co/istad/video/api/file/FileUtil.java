package co.istad.video.api.file;

import co.istad.video.api.file.web.FileDto;
import io.micrometer.common.lang.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

@Component
public class FileUtil {

    @Value("${file.base-uri}")
    private String fileBaseUri;

    @Value("${file.server-path}")
    private String fileServerPath;

    public FileDto save(MultipartFile file){

        if (!file.isEmpty()) {

            String fileName = file.getOriginalFilename();

            String extension = this.getFileExtension(fileName);
            Long size = file.getSize();
            fileName = String.format("%s.%s", UUID.randomUUID(), extension);
            String uri = String.format("%s%s", fileBaseUri, fileName);

            Path path = Paths.get(fileServerPath + fileName);

            try {
                Files.copy(file.getInputStream(), path);

                return new FileDto(
                        fileName,
                        uri,
                        extension,
                        size
                );

            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        } else {
            throw new RuntimeException();
        }

    }

    private String getFileExtension(@Nullable String fileName) {

        int index = Objects.requireNonNull(fileName).lastIndexOf(".");

        if (index > 0) {
            return fileName.substring(index + 1);
        }

        return "";

    }



}
