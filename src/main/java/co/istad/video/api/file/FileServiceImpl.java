package co.istad.video.api.file;

import co.istad.video.api.file.web.FileDto;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
@Service
@RequiredArgsConstructor

public class FileServiceImpl implements FileService{
    private final FileUtil fileUtil;
    @Override
    public FileDto uploadOne(MultipartFile file) {
        return fileUtil.save(file);
    }

    @Override
    public List<FileDto> uploadAll(List<MultipartFile> files) {
        List<FileDto> fileDtoList = new ArrayList<>();

        for (MultipartFile file : files) {
            fileDtoList.add(fileUtil.save(file));
        }

        return fileDtoList;

    }
}
