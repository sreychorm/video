package co.istad.video.api.playlist;

import co.istad.video.api.user.User;
import co.istad.video.api.user.UserProvider;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;

import java.time.LocalDateTime;
import java.util.List;

public class PlayListProvider {
    public String buildInsertSql(){
        return new SQL() {{

            INSERT_INTO("playlists");
            VALUES("uuid", "#{playlists.uuid}");
            VALUES("title", "#{playlists.title}");
            VALUES("owner_id", "#{playlists.ownerId}");
            VALUES("visibility", "#{playlists.visibility}");
            VALUES("created_at", "#{playlists.createdAt}");
            VALUES("updated_at", "#{playlists.updatedAt}");
            VALUES("thumbnail", "#{playlists.thumbnail}");
            VALUES("access_code", "#{playlists.accessCode}");
            VALUES("status", "#{playlists.status}");
        }}.toString();
    }

    public String buildSelectSql() {
        return new SQL() {{
            SELECT("*");
            FROM("playlists");
            WHERE("status = #{status}");
            ORDER_BY("id DESC");
        }}.toString();
    }

    public String buildSelectByUuidSql() {
        return new SQL() {{
            SELECT("*");
            FROM("playlists");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String buildSelectUserNames() {
        return new SQL() {{
            SELECT("*");
            FROM("playlists p");
            INNER_JOIN("users u ON p.owner_id = u.id");
            WHERE("p.id = #{id}");
        }}.toString();
    }
}
