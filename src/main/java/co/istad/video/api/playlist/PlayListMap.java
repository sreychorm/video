package co.istad.video.api.playlist;

import co.istad.video.api.playlist.web.PlayListDto;
import co.istad.video.api.playlist.web.SavePlayListDto;
import co.istad.video.api.user.User;
import co.istad.video.api.user.web.SaveUserDto;
import co.istad.video.api.user.web.UserDto;
import com.github.pagehelper.PageInfo;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PlayListMap {
    PlayList fromSavePlaylistDto(SavePlayListDto savePlayListDto);
  //  PlayListDto toPlaylistDto(PlayList playList);

    PageInfo<PlayListDto> toPlaylistDtoPageInfo(PageInfo<PlayList> playListPageInfo);
    PlayListDto toPlaylistDto(PlayList playList);

}
