package co.istad.video.api.playlist;

import co.istad.video.api.playlist.web.PlayListDto;
import co.istad.video.api.playlist.web.SavePlayListDto;
import co.istad.video.api.user.User;
import co.istad.video.api.user.web.UpdateUserDto;
import co.istad.video.api.user.web.UserDto;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
@Slf4j
public class PlayListServiceImpl implements PlayListService {
    private final PlayListMapper playListMapper;

    private final PlayListMap playListMap;

    @Override
    public PlayListDto savePlaylistDto(SavePlayListDto savePlayListDto) {

        PlayList playList = playListMap.fromSavePlaylistDto(savePlayListDto);
        playList.setUuid(UUID.randomUUID().toString());

        playListMapper.insert(playList);
        log.info("PlayList = {}", playList);

//        Optional<PlayList> userOp = PlayListMapper.selectById(playList.getId());
//
//        PlayList latestPlaylist = userOp.orElseThrow(() ->
//                new RuntimeException("User is not found"));
//        return playListMap.toPlaylistDto(latestPlaylist);
        return null;
    }

    @Override
    public PageInfo<PlayListDto> findPlaylists(int pageNum, int pageSize) {
        PageInfo<PlayList> playListPageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> playListMapper.select(true));

        return playListMap.toPlaylistDtoPageInfo(playListPageInfo);
    }


    @Override
    public PlayListDto findPlaylistByUuid(String uuid) {
        PlayList playlist = playListMapper.selectByUuid(uuid).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Playlist with this UUID is not exist in the db..!",
                        new Throwable("Something went wrong!")));
        return playListMap.toPlaylistDto(playlist);
    }

    @Override
    public void deleteByUuid(String uuid) {
        playListMapper.selectByUuid(uuid)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND,
                                "User with this UUID is not exist in the db..!",
                                new Throwable("Something went wrong!")));

        playListMapper.deleteByUuid(uuid);

    }

    @Override
    public UserDto updateByUuid(String uuid, UpdateUserDto updateUserDto) {
        return null;
    }
}
