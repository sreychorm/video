package co.istad.video.api.playlist;

import co.istad.video.api.user.User;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class PlayList {
    private Integer id;
    private String uuid;
    private String title;
    private Integer ownerId;
    private String visibility;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String thumbnail;
    private String accessCode;
    private Boolean status;
    List<User> users;


}
