package co.istad.video.api.playlist.web;

import co.istad.video.api.user.User;
import co.istad.video.api.user.web.UserDto;

import java.time.LocalDateTime;
import java.util.List;

public record PlayListDto(
        //Integer id,
        String title,
        //Integer ownerId,
        String visibility,
        LocalDateTime createdAt,
        LocalDateTime updatedAt,
        String thumbnail,
        //String accessCode,
        Boolean status,
        List<UserDto>users
) {
}
