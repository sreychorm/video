package co.istad.video.api.playlist.web;

import jakarta.validation.constraints.NotBlank;

import java.time.LocalDateTime;

public record SavePlayListDto(
        @NotBlank(message = "You have to fill title..!")
        String title,
      //  @NotBlank(message = "You have to fill owner Id..!")
        Integer ownerId,
        String visibility,
        Boolean status
) {
}
