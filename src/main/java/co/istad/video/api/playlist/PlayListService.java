package co.istad.video.api.playlist;

import co.istad.video.api.playlist.web.PlayListDto;
import co.istad.video.api.playlist.web.SavePlayListDto;
import co.istad.video.api.user.web.SaveUserDto;
import co.istad.video.api.user.web.UpdateUserDto;
import co.istad.video.api.user.web.UserDto;
import com.github.pagehelper.PageInfo;

public interface PlayListService {
    PlayListDto savePlaylistDto(SavePlayListDto savePlayListDto);


    PageInfo<PlayListDto> findPlaylists(int pageNum, int pageSize);

    PlayListDto findPlaylistByUuid(String uuid);

    void deleteByUuid(String uuid);

    UserDto updateByUuid(String uuid, UpdateUserDto updateUserDto);
}
