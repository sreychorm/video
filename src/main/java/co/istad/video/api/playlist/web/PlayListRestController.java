package co.istad.video.api.playlist.web;


import co.istad.video.api.auth.base.Rest;
import co.istad.video.api.playlist.PlayListServiceImpl;
import co.istad.video.api.user.UserServiceImpl;
import co.istad.video.api.user.web.SaveUserDto;
import co.istad.video.api.user.web.UserDto;
import com.github.pagehelper.PageInfo;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/playlists")
public class PlayListRestController {
    private final PlayListServiceImpl playListService;

    @PostMapping
    Rest<?> savePlaylist(@Valid @RequestBody SavePlayListDto savePlayListDto) {

        var playlistDto = playListService.savePlaylistDto(savePlayListDto);

        return Rest.builder()
                .status(true)
                .message("You have retrieved user successfully")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(playlistDto)
                .build();
    }

    @GetMapping
    Rest<?> findPlayLists(@RequestParam(required = false, defaultValue = "20") int pageNum,
                      @RequestParam(required = false, defaultValue = "1") int pageSize) {

        PageInfo<PlayListDto> PlayListDtoPageInfo = playListService.findPlaylists(pageNum, pageSize);

        return Rest.builder()
                .status(true)
                .message("You have retrieved users successfully")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(PlayListDtoPageInfo)
                .build();
    }

    @GetMapping("/{uuid}")
    Rest<?> findPlaylistByUuid(@PathVariable("uuid") String uuid) {

        PlayListDto playListDto = playListService.findPlaylistByUuid(uuid);

        return Rest.builder()
                .status(true)
                .message("You have retrieved an Playlist successfully")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(playListDto)
                .build();
    }

    @DeleteMapping("/{uuid}")
    Rest<?> deletePlaylistByUuid(@PathVariable("uuid") String uuid) {

        playListService.deleteByUuid(uuid);

        return Rest.builder()
                .status(true)
                .message("You have deleted an user successfully")
                .code(HttpStatus.NO_CONTENT.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
    }
}
