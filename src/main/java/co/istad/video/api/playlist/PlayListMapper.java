package co.istad.video.api.playlist;

import co.istad.video.api.user.Role;
import co.istad.video.api.user.User;
import co.istad.video.api.user.UserProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface PlayListMapper {

    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    @InsertProvider(type = PlayListProvider.class,method = "buildInsertSql")
    void insert(@Param("playlists") PlayList playList);

    @SelectProvider(type = PlayListProvider.class, method = "buildSelectSql")
    @Results(id = "playlistResultMapper", value = {
//            @Result(property = "familyName", column = "family_name"),
//            @Result(property = "givenName", column = "given_name"),
            //@Result(property = "createdAt", column = "created_at"),
            @Result(property = "users", column = "id", many = @Many(select = "selectUserNames"))
    })
    List<PlayList> select(@Param("status") Boolean status);

    @SelectProvider(type = PlayListProvider.class, method = "buildSelectByUuidSql")
   // @ResultMap(value = "playlistResultMapper")
    Optional<PlayList> selectByUuid(@Param("uuid") String uuid);

    @Delete("DELETE FROM playlists WHERE uuid = #{uuid}")
    void deleteByUuid(@Param("uuid") String uuid);

    @SelectProvider(type = PlayListProvider.class, method = "buildSelectUserNames")
    List<User> selectUserNames(@Param("id") Integer id);
}
