package co.istad.video.mail;

public interface MailService {
     void sendMail(Mail<?>mail);
}
